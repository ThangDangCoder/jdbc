package vn.edu.vnuk.jdbc.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import vn.edu.vnuk.jdbc.ConnectionFactory;

public class CreateMotorbikes {
	
	public static void main (String[]args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		String sqlQuery = "CREATE TABLE IF NOT EXISTS motorbikes (id bigint NOT NULL AUTO_INCREMENT,"
				+ " brand varchar(255), model varchar(255), engine_size int, gear_box varchar(255), PRIMARY KEY(id))";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
		
			statement.executeUpdate(sqlQuery);
			statement.close();
			System.out.println("New table in DB!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
			System.out.println("Done!");
		}
	}
	

}
