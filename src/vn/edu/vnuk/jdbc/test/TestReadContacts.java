package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.List;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestReadContacts {
	
	public static void main(String[] args) throws SQLException {
		ContactDao contactDao  = new ContactDao();
		List<Contact> contacts = contactDao.read();
		
		for(Contact contact:contacts) {
			System.out.print("ID : " + contact.getId());
			System.out.print(", Name : " + contact.getName());
			System.out.print(", Email : " + contact.getEmail());
			System.out.print(", Address: " + contact.getAddress());
			System.out.println(", DOB: " + contact.getDateOfBirth());
		}		
	}

}
