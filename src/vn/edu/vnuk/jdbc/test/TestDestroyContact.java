package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.ContactDao;

public class TestDestroyContact {

	public static void main(String[] args) throws SQLException {
		ContactDao contactDao = new ContactDao();
		contactDao.destroy(1);
	}

}
