package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestReadMotorbike {

	public static void main(String[] args) throws SQLException {
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		Motorbike motorbike = motorbikeDao.read(1);
		
		System.out.print("ID : " + motorbike.getId());
		System.out.print(", Brand : " + motorbike.getBrand());
		System.out.print(", Model : " + motorbike.getModel());
		System.out.print(", Engine size : " + motorbike.getEngineSize());
		System.out.print(", Gear box : " + motorbike.getGearBox());
	}

}
