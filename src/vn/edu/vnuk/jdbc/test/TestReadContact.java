package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestReadContact {

	public static void main(String[] args) throws SQLException {
		ContactDao contactDao = new ContactDao();
		Contact contact = contactDao.read(1);
		
		System.out.print("ID : " + contact.getId());
		System.out.print(", Name : " + contact.getName());
		System.out.print(", Email : " + contact.getEmail());
		System.out.print(", Address: " + contact.getAddress());
		System.out.println(", DOB: " + contact.getDateOfBirth());
			
	}

}
