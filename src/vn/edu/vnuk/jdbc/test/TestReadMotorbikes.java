package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.List;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestReadMotorbikes {

	public static void main(String[] args) throws SQLException {
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		List<Motorbike> motorbikes = motorbikeDao.read();
		
		for(Motorbike motorbike:motorbikes) {
			System.out.print("ID : " + motorbike.getId());
			System.out.print(", Brand : " + motorbike.getBrand());
			System.out.print(", Model : " + motorbike.getModel());
			System.out.print(", Engine size : " + motorbike.getEngineSize());
			System.out.println(", Gear box : " + motorbike.getGearBox());
		}
	}

}
