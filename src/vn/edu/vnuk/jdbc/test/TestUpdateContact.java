package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestUpdateContact {

	public static void main(String[] args) throws SQLException {
		ContactDao contactDao = new ContactDao();
		Contact contact = new Contact();
		contact.setId(3);
		contact.setName("ABC");
		contact.setEmail("vnuk@dn.gmail");
		contact.setAddress("UK");
		contactDao.update(contact);
	}

}
