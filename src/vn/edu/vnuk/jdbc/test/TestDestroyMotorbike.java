package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;

public class TestDestroyMotorbike {

	public static void main(String[] args) throws SQLException {
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		motorbikeDao.destroy(3);
	}

}
