package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestCreateMotorbike {

	public static void main(String[] args) throws SQLException {
		Motorbike motorbike = new Motorbike();
		motorbike.setBrand("Honda");
		motorbike.setModel("WaveAlpha");
		motorbike.setEngineSize(135);
		motorbike.setGearBox("Semi");
		
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		motorbikeDao.create(motorbike);
	}

}
