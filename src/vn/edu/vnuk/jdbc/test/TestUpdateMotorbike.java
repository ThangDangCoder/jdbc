package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestUpdateMotorbike {

	public static void main(String[] args) throws SQLException {

		MotorbikeDao motorbikeDao = new MotorbikeDao();
		Motorbike motorbike = new Motorbike();
		motorbike.setId(3);
		motorbike.setBrand("Lamborgini");
		motorbike.setModel("MCLaren");
		motorbike.setEngineSize(1000);
		motorbike.setGearBox("Unknow");
		motorbikeDao.update(motorbike);
	}

}
