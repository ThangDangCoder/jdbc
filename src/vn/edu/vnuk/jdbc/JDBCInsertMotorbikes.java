package vn.edu.vnuk.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class JDBCInsertMotorbikes {

	public static void main (String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		String sqlQuery = "insert into motorbikes (brand, model, engine_size, gear_box) "
				+ "values (?, ?, ?, ?)";
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, "SYM");
			statement.setString(2, "Angela");
			statement.setInt(3, 50);
			statement.setString(4, "Semi-Auto");
			statement.execute();
			statement.close();
			System.out.println("New Record in DB!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connection.close();
			System.out.println("Done!");
		}
	}

}
