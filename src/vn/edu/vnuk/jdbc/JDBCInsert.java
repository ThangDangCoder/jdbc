package vn.edu.vnuk.jdbc;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

public class JDBCInsert {

	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		String sqlQuery = "insert into contacts (name, email, address, date_of_birth ) "
				+ "values (?, ?, ?, ?)";
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, "Thang");
			statement.setString(2, "thang.dang@gmail.com");
			statement.setString(3, "Danang");
			statement.setDate(4,
					new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
			statement.execute();
			statement.close();
			System.out.println("New Record in DB!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			connection.close();
			System.out.println("Done!");
		}
		
		
	}

}
